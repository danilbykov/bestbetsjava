package club.bestbets.javaapi;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import club.bestbets.javaapi.model.Bookmaker;
import club.bestbets.javaapi.model.Country;
import club.bestbets.javaapi.model.Match;
import club.bestbets.javaapi.model.MatchHistory;
import club.bestbets.javaapi.model.MatchSnapshot;
import club.bestbets.javaapi.model.Team;
import club.bestbets.javaapi.model.Tournament;

public class BestBetsApi {

  private final String root = "https://ec2-52-56-215-130.eu-west-2.compute.amazonaws.com:8443/rest/";
  private final ObjectMapper mapper = new ObjectMapper();

  public BestBetsApi() {
  }

  public List<Bookmaker> loadBookmakers() throws IOException {
    final URL url = new URL(root + "bookmakers");
    return mapper.readValue(url,
      mapper.getTypeFactory().constructCollectionType(List.class, Bookmaker.class)
    );
  }

  public List<Country> loadCountries() throws IOException {
    final URL url = new URL(root + "countries");
    return mapper.readValue(url,
      mapper.getTypeFactory().constructCollectionType(List.class, Country.class)
    );
  }

  public List<Team> loadTeams(Country country) throws IOException {
    return loadTeams(country.getId());
  }
  public List<Team> loadTeams(int countryId) throws IOException {
    final URL url = new URL(root + "countries/" + countryId + "/teams");
    return mapper.readValue(url,
      mapper.getTypeFactory().constructCollectionType(List.class, Team.class)
    );
  }

  public List<Tournament> loadTournaments() throws IOException {
    final URL url = new URL(root + "tournaments");
    return mapper.readValue(url,
      mapper.getTypeFactory().constructCollectionType(List.class, Tournament.class)
    );
  }
  
  public List<MatchSnapshot> loadTournamentSnapshot(Tournament tournament) throws IOException {
    return loadTournamentSnapshot(tournament.getId());
  }
  public List<MatchSnapshot> loadTournamentSnapshot(int tournamentId) throws IOException {
    final URL url = new URL(root + "tournaments/" + tournamentId + "/matches/open");
    return mapper.readValue(url,
      mapper.getTypeFactory().constructCollectionType(List.class, MatchSnapshot.class)
    );
  }

  public HistoryIterator makeHistoryIterator(Tournament tournament, int count) throws IOException {
    return makeHistoryIterator(tournament.getId(), count, Long.MAX_VALUE);
  }
  public HistoryIterator makeHistoryIterator(Tournament tournament, int count, long from) throws IOException {
    return makeHistoryIterator(tournament.getId(), count, from);
  }
  public HistoryIterator makeHistoryIterator(int tournamentId, int count) throws IOException {
    return makeHistoryIterator(tournamentId, count, Long.MAX_VALUE);
  }
  public HistoryIterator makeHistoryIterator(int tournamentId, int count, long from) throws IOException {
    return new HistoryIteratorImpl(tournamentId, count, from);
  }
  
  public MatchHistory loadMatchHistory(Tournament tournament, long matchId) throws IOException {
    return loadMatchHistory(tournament.getId(), matchId);
  }
  public MatchHistory loadMatchHistory(int tournamentId, long matchId) throws IOException {
    final URL url = new URL(root + "tournaments/" + tournamentId + "/matches/history/" + matchId);
    return mapper.readValue(url, MatchHistory.class);
  }
  
  public MatchHistory loadOpenMatchHistory(Tournament tournament, long matchId) throws IOException {
    return loadOpenMatchHistory(tournament.getId(), matchId);
  }
  public MatchHistory loadOpenMatchHistory(int tournamentId, long matchId) throws IOException {
    final URL url = new URL(root + "tournaments/" + tournamentId + "/matches/open/" + matchId);
    return mapper.readValue(url, MatchHistory.class);
  }
  
  
  interface HistoryIterator {
    public boolean hasNext();
    public List<Match> next() throws IOException;
  }
  private class HistoryIteratorImpl implements HistoryIterator {
    private final int tournamentId;
    private final int count;
    private long from;
    private boolean hasNext = true;
    
    public HistoryIteratorImpl(int tournamentId, int count, long from) {
      this.tournamentId = tournamentId;
      this.count = count;
      this.from = from;
    }
    
    @Override
    public boolean hasNext() {
      return hasNext;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Match> next() throws IOException {
      final URL url = new URL(root + "tournaments/" + tournamentId + "/matches/history?from=" + from + "&count=" + count);
      final List<Match> result = (List<Match>)mapper.readValue(url,
        mapper.getTypeFactory().constructCollectionType(List.class, Match.class)
      );
      if (result.isEmpty()) {
        hasNext = false;
      } else {
        from = result.get(result.size() - 1).getStart();
      }
      return result;
    }
  }
}
