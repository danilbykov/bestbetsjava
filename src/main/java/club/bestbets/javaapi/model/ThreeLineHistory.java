package club.bestbets.javaapi.model;

import static java.util.Collections.unmodifiableList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ThreeLineHistory {
  private final List<HistoricalBet> homeWin;
  private final List<HistoricalBet> draw;
  private final List<HistoricalBet> awayWin;
  
  public ThreeLineHistory(
    @JsonProperty("homeWin") List<HistoricalBet> homeWin,
    @JsonProperty("draw") List<HistoricalBet> draw,
    @JsonProperty("awayWin") List<HistoricalBet> awayWin
  ) {
    super();
    this.homeWin = unmodifiableList(homeWin);
    this.draw = unmodifiableList(draw);
    this.awayWin = unmodifiableList(awayWin);
  }
  
  public List<HistoricalBet> getHomeWin() {
    return homeWin;
  }
  public List<HistoricalBet> getDraw() {
    return draw;
  }
  public List<HistoricalBet> getAwayWin() {
    return awayWin;
  }
  
  @Override
  public String toString() {
    return String.format("(1=%s x=%s 2=%s)", homeWin, draw, awayWin);
  }
}