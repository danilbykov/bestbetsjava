package club.bestbets.javaapi.model;

import static java.util.Collections.unmodifiableList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TwoLineHistory {
  private final List<HistoricalBet> homeWin;
  private final List<HistoricalBet> awayWin;
  
  public TwoLineHistory(
    @JsonProperty("homeWin") List<HistoricalBet> homeWin,
    @JsonProperty("awayWin") List<HistoricalBet> awayWin
  ) {
    super();
    this.homeWin = unmodifiableList(homeWin);
    this.awayWin = unmodifiableList(awayWin);
  }
  
  public List<HistoricalBet> getHomeWin() {
    return homeWin;
  }
  public List<HistoricalBet> getAwayWin() {
    return awayWin;
  }
  
  @Override
  public String toString() {
    return String.format("(1=%s 2=%s)", homeWin, awayWin);
  }
}