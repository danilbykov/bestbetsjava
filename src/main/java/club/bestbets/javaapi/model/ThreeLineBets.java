package club.bestbets.javaapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ThreeLineBets {
  private final float homeWin;
  private final float draw;
  private final float awayWin;
  
  public ThreeLineBets(
    @JsonProperty("homeWin") float homeWin,
    @JsonProperty("draw") float draw,
    @JsonProperty("awayWin") float awayWin
  ) {
    super();
    this.homeWin = homeWin;
    this.draw = draw;
    this.awayWin = awayWin;
  }
  
  public float getHomeWin() {
    return homeWin;
  }
  public float getDraw() {
    return draw;
  }
  public float getAwayWin() {
    return awayWin;
  }
  
  @Override
  public String toString() {
    return String.format("(1=%.3f x=%.3f 2=%.3f)", homeWin, draw, awayWin);
  }
}