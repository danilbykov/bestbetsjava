package club.bestbets.javaapi.model;

import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BookmakerHistory {
  private final List<HistoricalBet> homeWin;
  private final List<HistoricalBet> draw;
  private final List<HistoricalBet> awayWin;
  private final List<HistoricalBet> notHomeWin;
  private final List<HistoricalBet> notDraw;
  private final List<HistoricalBet> notAwayWin;
  private final List<HistoricalBet> drawNoBetHome;
  private final List<HistoricalBet> drawNoBetAway;
  private final List<HistoricalBet> bothTeamsScore;
  private final List<HistoricalBet> bothTeamsNotScore;
  private final List<HistoricalBet> oddGoals;
  private final List<HistoricalBet> evenGoals;
  private final Map<String, List<HistoricalBet>> correctScores;
  private final Map<String, List<HistoricalBet>> overTotals;
  private final Map<String, List<HistoricalBet>> underTotals;
  private final Map<String, TwoLineHistory> asianHandicaps;
  private final Map<String, ThreeLineHistory> europeanHandicaps;

  @JsonCreator
  public BookmakerHistory(
    @JsonProperty("homeWin") List<HistoricalBet> homeWin,
    @JsonProperty("draw") List<HistoricalBet> draw,
    @JsonProperty("awayWin") List<HistoricalBet> awayWin,
    @JsonProperty("notHomeWin") List<HistoricalBet> notHomeWin,
    @JsonProperty("notDraw") List<HistoricalBet> notDraw,
    @JsonProperty("notAwayWin") List<HistoricalBet> notAwayWin,
    @JsonProperty("drawNoBetHome") List<HistoricalBet> drawNoBetHome,
    @JsonProperty("drawNoBetAway") List<HistoricalBet> drawNoBetAway,
    @JsonProperty("bothTeamsScore") List<HistoricalBet> bothTeamsScore,
    @JsonProperty("bothTeamsNotScore") List<HistoricalBet> bothTeamsNotScore,
    @JsonProperty("oddGoals") List<HistoricalBet> oddGoals,
    @JsonProperty("evenGoals") List<HistoricalBet> evenGoals,
    @JsonProperty("correctScores") Map<String, List<HistoricalBet>> correctScores,
    @JsonProperty("overTotals") Map<String, List<HistoricalBet>> overTotals,
    @JsonProperty("underTotals") Map<String, List<HistoricalBet>> underTotals,
    @JsonProperty("asianHandicaps") Map<String, TwoLineHistory> asianHandicaps,
    @JsonProperty("europeanHandicaps") Map<String, ThreeLineHistory> europeanHandicaps
  ) {
    this.homeWin = unmodifiableList(homeWin);
    this.draw = unmodifiableList(draw);
    this.awayWin = unmodifiableList(awayWin);
    this.notHomeWin = unmodifiableList(notHomeWin);
    this.notDraw = unmodifiableList(notDraw);
    this.notAwayWin = unmodifiableList(notAwayWin);
    this.drawNoBetHome = unmodifiableList(drawNoBetHome);
    this.drawNoBetAway = unmodifiableList(drawNoBetAway);
    this.bothTeamsScore = unmodifiableList(bothTeamsScore);
    this.bothTeamsNotScore = unmodifiableList(bothTeamsNotScore);
    this.oddGoals = unmodifiableList(oddGoals);
    this.evenGoals = unmodifiableList(evenGoals);
    this.correctScores = unmodifiableMap(correctScores);
    this.overTotals = unmodifiableMap(overTotals);
    this.underTotals = unmodifiableMap(underTotals);
    this.asianHandicaps = unmodifiableMap(asianHandicaps);
    this.europeanHandicaps = unmodifiableMap(europeanHandicaps);
  }

  public List<HistoricalBet> getHomeWin() {
    return homeWin;
  }
  public List<HistoricalBet> getDraw() {
    return draw;
  }
  public List<HistoricalBet> getAwayWin() {
    return awayWin;
  }
  public List<HistoricalBet> getNotHomeWin() {
    return notHomeWin;
  }
  public List<HistoricalBet> getNotDraw() {
    return notDraw;
  }
  public List<HistoricalBet> getNotAwayWin() {
    return notAwayWin;
  }
  public List<HistoricalBet> getDrawNoBetHome() {
    return drawNoBetHome;
  }
  public List<HistoricalBet> getDrawNoBetAway() {
    return drawNoBetAway;
  }
  public List<HistoricalBet> getBothTeamsScore() {
    return bothTeamsScore;
  }
  public List<HistoricalBet> getBothTeamsNotScore() {
    return bothTeamsNotScore;
  }
  public List<HistoricalBet> getOddGoals() {
    return oddGoals;
  }
  public List<HistoricalBet> getEvenGoals() {
    return evenGoals;
  }
  public Map<String, List<HistoricalBet>> getCorrectScores() {
    return correctScores;
  }
  public Map<String, List<HistoricalBet>> getOverTotals() {
    return overTotals;
  }
  public Map<String, List<HistoricalBet>> getUnderTotals() {
    return underTotals;
  }
  public Map<String, TwoLineHistory> getAsianHandicaps() {
    return asianHandicaps;
  }
  public Map<String, ThreeLineHistory> getEuropeanHandicaps() {
    return europeanHandicaps;
  }
  
  @Override
  public String toString() {
    return String.format("<BookmakerHistory "
        + "1=%s x=%s 2=%s "
        + "1x=%s 12=%s x2=%s "
        + "p1=%s p2=%s "
        + "score=%s notscore=%s "
        + "odd=%s even=%s />",
      homeWin, draw, awayWin,
      notAwayWin, notDraw, notHomeWin,
      drawNoBetHome, drawNoBetAway,
      bothTeamsScore, bothTeamsNotScore,
      oddGoals, evenGoals
    );
  }
  
  public String dump(String linePrefix) {
    final StringBuilder sb = new StringBuilder();
    sb.append("<BookmakerHistory")
      .append("\n").append(linePrefix).append("1=").append(homeWin)
      .append("\n").append(linePrefix).append("x=").append(draw)
      .append("\n").append(linePrefix).append("2=").append(awayWin)
      .append("\n").append(linePrefix).append("1x=").append(notAwayWin)
      .append("\n").append(linePrefix).append("12=").append(notDraw)
      .append("\n").append(linePrefix).append("x2=").append(notHomeWin)
      .append("\n").append(linePrefix).append("w1=").append(drawNoBetHome)
      .append("\n").append(linePrefix).append("w2=").append(drawNoBetAway)
      .append("\n").append(linePrefix).append("score=").append(bothTeamsScore)
      .append("\n").append(linePrefix).append("notscore=").append(bothTeamsNotScore)
      .append("\n").append(linePrefix).append("odd=").append(oddGoals)
      .append("\n").append(linePrefix).append("even=").append(evenGoals);
    dumpMap(sb, linePrefix, "correctScores", correctScores);
    dumpMap(sb, linePrefix, "underTotals", overTotals);
    dumpMap(sb, linePrefix, "overTotals", underTotals);
    dumpMap(sb, linePrefix, "asianHandicaps", asianHandicaps);
    dumpMap(sb, linePrefix, "europeanHandicaps", europeanHandicaps);
    sb.append("\n").append(linePrefix).append(" />");
    return sb.toString();
  }
  
  private static void dumpMap(StringBuilder sb, String linePrefix, String displayName, Map<String, ?> map) {
    sb.append("\n").append(linePrefix).append(displayName).append("=[");
    for (Map.Entry<String, ?> e: map.entrySet()) {
//      if (!e.getValue().isEmpty()) {
        sb.append("\n\t").append(linePrefix).append(e.getKey()).append("=").append(e.getValue());
//      }
    }
    sb.append("\n").append(linePrefix).append("]");
  }
}