package club.bestbets.javaapi.model;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Match {
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

  private final long id;
  private final int homeTeamId;
  private final int awayTeamId;
  private final long start;
  private final int tournamentId;

  @JsonCreator
  public Match(
    @JsonProperty("id") long id,
    @JsonProperty("homeTeamId") int homeTeamId,
    @JsonProperty("awayTeamId") int awayTeamId,
    @JsonProperty("start") long start,
    @JsonProperty("tournamentId") int tournamentId
  ) {
    this.id = id;
    this.homeTeamId = homeTeamId;
    this.awayTeamId = awayTeamId;
    this.start = start;
    this.tournamentId = tournamentId;
  }

  public long getId() {
    return id;
  }
  public int getHomeTeamId() {
    return homeTeamId;
  }
  public int getAwayTeamId() {
    return awayTeamId;
  }
  public long getStart() {
    return start;
  }
  public int getTournamentId() {
    return tournamentId;
  }

  @Override
  public String toString() {
    return String.format("<Match id=%d teams=%d vs %d tournament=%d start=%s />",
       id, homeTeamId, awayTeamId, tournamentId, dateFormat.format(start));
  }
}