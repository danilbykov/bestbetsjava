package club.bestbets.javaapi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Team {
  private final int id;
  private final String name;

  @JsonCreator
  public Team(
    @JsonProperty("id") int id,
    @JsonProperty("name") String name
  ) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }
  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return String.format("<Team id=%d name=%s />", id, name);
  }
}