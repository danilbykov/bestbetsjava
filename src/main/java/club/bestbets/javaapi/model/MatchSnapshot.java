package club.bestbets.javaapi.model;

import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchSnapshot {
  private final Match match;
  private final Map<String, BookmakerSnapshot> bookmakers;

  @JsonCreator
  public MatchSnapshot(
    @JsonProperty("match") Match match,
    @JsonProperty("bookmakers") Map<String, BookmakerSnapshot> bookmakers
  ) {
    this.match = match;
    this.bookmakers = Collections.unmodifiableMap(bookmakers);
  }

  public Match getMatch() {
    return match;
  }

  public BookmakerSnapshot getBookmakerSnapshot(Bookmaker bookmaker) {
    return getBookmakerSnapshot(bookmaker.getName());
  }
  public BookmakerSnapshot getBookmakerSnapshot(String name) {
    return bookmakers.get(name);
  }

  public Map<String, BookmakerSnapshot> getAllSnapshots() {
    return this.bookmakers;
  }
  
  @Override
  public String toString() {
    return String.format("<CurrentSnapshot match=%s bookmakers=%s />", match, bookmakers.keySet());
  }
  
  public String dump() {
    final StringBuilder sb = new StringBuilder();
    sb.append("<CurrentSnapshot match=")
      .append(match)
      .append(" ")
      .append("bookmakers=")
      .append("\n");
    for (Map.Entry<String, BookmakerSnapshot> e: bookmakers.entrySet()) {
      sb.append("\t")
        .append(e.getKey())
        .append("=")
        .append(e.getValue())
        .append("\n");
    }
    sb.append(" />");
    return sb.toString();
  }
}