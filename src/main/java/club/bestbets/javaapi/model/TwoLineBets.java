package club.bestbets.javaapi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TwoLineBets {
  private final float homeWin;
  private final float awayWin;

  @JsonCreator
  public TwoLineBets(
    @JsonProperty("homeWin") float homeWin,
    @JsonProperty("awayWin") float awayWin
  ) {
    super();
    this.homeWin = homeWin;
    this.awayWin = awayWin;
  }
  
  public float getHomeWin() {
    return homeWin;
  }
  public float getAwayWin() {
    return awayWin;
  }
  
  @Override
  public String toString() {
    return String.format("(w1=%.3f w2=%.3f)", homeWin, awayWin);
  }
}