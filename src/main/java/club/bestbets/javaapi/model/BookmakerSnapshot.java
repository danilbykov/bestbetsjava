package club.bestbets.javaapi.model;

import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BookmakerSnapshot {
  private final float homeWin;
  private final float draw;
  private final float awayWin;
  private final float notHomeWin;
  private final float notDraw;
  private final float notAwayWin;
  private final float drawNoBetHome;
  private final float drawNoBetAway;
  private final float bothTeamsScore;
  private final float bothTeamsNotScore;
  private final float oddGoals;
  private final float evenGoals;
  private final Map<String, Float> correctScores;
  private final Map<String, Float> overTotals;
  private final Map<String, Float> underTotals;
  private final Map<String, TwoLineBets> asianHandicaps;
  private final Map<String, ThreeLineBets> europeanHandicaps;

  @JsonCreator
  public BookmakerSnapshot(
    @JsonProperty("homeWin") float homeWin,
    @JsonProperty("draw") float draw,
    @JsonProperty("awayWin") float awayWin,
    @JsonProperty("notHomeWin") float notHomeWin,
    @JsonProperty("notDraw") float notDraw,
    @JsonProperty("notAwayWin") float notAwayWin,
    @JsonProperty("drawNoBetHome") float drawNoBetHome,
    @JsonProperty("drawNoBetAway") float drawNoBetAway,
    @JsonProperty("bothTeamsScore") float bothTeamsScore,
    @JsonProperty("bothTeamsNotScore") float bothTeamsNotScore,
    @JsonProperty("oddGoals") float oddGoals,
    @JsonProperty("evenGoals") float evenGoals,
    @JsonProperty("correctScores") Map<String, Float> correctScores,
    @JsonProperty("overTotals") Map<String, Float> overTotals,
    @JsonProperty("underTotals") Map<String, Float> underTotals,
    @JsonProperty("asianHandicaps") Map<String, TwoLineBets> asianHandicaps,
    @JsonProperty("europeanHandicaps") Map<String, ThreeLineBets> europeanHandicaps
  ) {
    this.homeWin = homeWin;
    this.draw = draw;
    this.awayWin = awayWin;
    this.notHomeWin = notHomeWin;
    this.notDraw = notDraw;
    this.notAwayWin = notAwayWin;
    this.drawNoBetHome = drawNoBetHome;
    this.drawNoBetAway = drawNoBetAway;
    this.bothTeamsScore = bothTeamsScore;
    this.bothTeamsNotScore = bothTeamsNotScore;
    this.oddGoals = oddGoals;
    this.evenGoals = evenGoals;
    this.correctScores = Collections.unmodifiableMap(correctScores);
    this.overTotals = Collections.unmodifiableMap(overTotals);
    this.underTotals = Collections.unmodifiableMap(underTotals);
    this.asianHandicaps = Collections.unmodifiableMap(asianHandicaps);
    this.europeanHandicaps = Collections.unmodifiableMap(europeanHandicaps);
  }

  public float getHomeWin() {
    return homeWin;
  }
  public float getDraw() {
    return draw;
  }
  public float getAwayWin() {
    return awayWin;
  }
  public float getNotHomeWin() {
    return notHomeWin;
  }
  public float getNotDraw() {
    return notDraw;
  }
  public float getNotAwayWin() {
    return notAwayWin;
  }
  public float getDrawNoBetHome() {
    return drawNoBetHome;
  }
  public float getDrawNoBetAway() {
    return drawNoBetAway;
  }
  public float getBothTeamsScore() {
    return bothTeamsScore;
  }
  public float getBothTeamsNotScore() {
    return bothTeamsNotScore;
  }
  public float getOddGoals() {
    return oddGoals;
  }
  public float getEvenGoals() {
    return evenGoals;
  }
  public Map<String, Float> getCorrectScores() {
    return correctScores;
  }
  public Map<String, Float> getOverTotals() {
    return overTotals;
  }
  public Map<String, Float> getUnderTotals() {
    return underTotals;
  }
  public Map<String, TwoLineBets> getAsianHandicaps() {
    return asianHandicaps;
  }
  public Map<String, ThreeLineBets> getEuropeanHandicaps() {
    return europeanHandicaps;
  }
  
  @Override
  public String toString() {
    return String.format("<BookmakerSnapshot "
        + "1=%.3f x=%.3f 2=%.3f "
        + "1x=%.3f 12=%.3f x2=%.3f "
        + "w1=%.3f w2=%.3f "
        + "score=%.3f notscore=%.3f "
        + "odd=%.3f even=%.3f\n%s\n%s\n%s\n%s\n%s/>",
      homeWin, draw, awayWin,
      notAwayWin, notDraw, notHomeWin,
      drawNoBetHome, drawNoBetAway,
      bothTeamsScore, bothTeamsNotScore,
      oddGoals, evenGoals,
      mapToString(correctScores, "scores"),
      mapToString(overTotals, "overTotals"),
      mapToString(underTotals, "underTotals"),
      mapWithObjectsToString(asianHandicaps, "asianHandicaps"),
      mapWithObjectsToString(europeanHandicaps, "europeanHandicaps")
    );
  }
  
  private String mapToString(Map<String, Float> map, String name) {
    final StringBuilder sb = new StringBuilder();
    sb.append(name).append("=");
    for (Map.Entry<String, Float> e: map.entrySet()) {
      sb.append(e.getKey())
        .append("->")
        .append(String.format("%.3f", e.getValue()))
        .append(" ");
    }
    return sb.toString();
  }
  private String mapWithObjectsToString(Map<String, ?> map, String name) {
    final StringBuilder sb = new StringBuilder();
    sb.append(name).append("=");
    for (Map.Entry<String, ?> e: map.entrySet()) {
      sb.append("\"")
        .append(e.getKey())
        .append("\"->")
        .append(e.getValue())
        .append(" ");
    }
    return sb.toString();
  }
}