package club.bestbets.javaapi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Tournament {
  private final int id;
  private final String name;
  private final String country;

  @JsonCreator
  public Tournament(
    @JsonProperty("id") int id,
    @JsonProperty("name") String name,
    @JsonProperty("country") String country
  ) {
    this.id = id;
    this.name = name;
    this.country = country;
  }

  public int getId() {
    return id;
  }
  public String getName() {
    return name;
  }
  public String getCountry() {
    return country;
  }

  @Override
  public String toString() {
    return String.format("<Tournament id=%d name=%s country=%s />", id, name, country);
  }
}