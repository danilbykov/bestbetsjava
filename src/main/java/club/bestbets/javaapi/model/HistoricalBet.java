package club.bestbets.javaapi.model;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HistoricalBet {
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd HH:mm");
  
  private final long time;
  private final float value;
  
  @JsonCreator
  public HistoricalBet(
    @JsonProperty("time") long time,
    @JsonProperty("value") float value
  ) {
    super();
    this.time = time;
    this.value = value;
  }

  public long getTime() {
    return time;
  }
  public float getValue() {
    return value;
  }
  
  @Override
  public String toString() {
    return String.format("(%.3f %s)", value, dateFormat.format(time));
  }
}