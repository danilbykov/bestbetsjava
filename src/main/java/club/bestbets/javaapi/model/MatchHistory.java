package club.bestbets.javaapi.model;

import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchHistory {
  private final Match match;
  private final Map<String, BookmakerHistory> bookmakers;

  @JsonCreator
  public MatchHistory(
    @JsonProperty("match") Match match,
    @JsonProperty("bookmakers") Map<String, BookmakerHistory> bookmakers
  ) {
    this.match = match;
    this.bookmakers = Collections.unmodifiableMap(bookmakers);
  }

  public Match getMatch() {
    return match;
  }

  public BookmakerHistory getBookmakerSnapshot(Bookmaker bookmaker) {
    return getBookmakerSnapshot(bookmaker.getName());
  }
  public BookmakerHistory getBookmakerSnapshot(String name) {
    return bookmakers.get(name);
  }

  public Map<String, BookmakerHistory> getAllSnapshots() {
    return this.bookmakers;
  }
  
  @Override
  public String toString() {
    return String.format("<MatchHistory match=%s bookmakers=%s />", match, bookmakers.keySet());
  }
  
  public String dump() {
    final StringBuilder sb = new StringBuilder();
    sb.append("<MatchHistory match=")
      .append(match)
      .append(" ")
      .append("bookmakers=")
      .append("\n");
    for (Map.Entry<String, BookmakerHistory> e: bookmakers.entrySet()) {
      sb.append("\t")
        .append(e.getKey())
        .append("=")
        .append(e.getValue().dump("\t\t"))
        .append("\n");
    }
    sb.append(" />");
    return sb.toString();
  }
}