package club.bestbets.javaapi;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import club.bestbets.javaapi.BestBetsApi.HistoryIterator;
import club.bestbets.javaapi.model.Match;
import club.bestbets.javaapi.model.MatchHistory;
import club.bestbets.javaapi.model.MatchSnapshot;
import club.bestbets.javaapi.model.Tournament;

public class Examples {

  static {
    final TrustManager[] trustManagers = new TrustManager[] {new X509TrustManager() {
      @Override
      public void checkClientTrusted(X509Certificate[] chain, String authType) {}
      @Override
      public void checkServerTrusted(X509Certificate[] chain, String authType) {}
      @Override
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }
    }};
    try {
      final SSLContext context = SSLContext.getInstance("SSL");
      context.init(null, trustManagers, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
    } catch (GeneralSecurityException e) {
      throw new IllegalStateException(e.getMessage(), e);
    }
    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
      @Override
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    });
  }

  public static void main(String[] args) throws IOException {
    final BestBetsApi api = new BestBetsApi();
    System.out.println(api.loadBookmakers());
    System.out.println(api.loadCountries());
    System.out.println(api.loadTournaments());

    final Tournament tournament = api.loadTournaments().get(12);
    
//    dumpOpenMatchSnapshots(api, tournament);
    
//    dumpOpenMatchHistory(api, tournament, 101);
    
//    iterateOverHistoricy(api, tournament, 10);
    
    dumpHistoryMatchHistory(api, tournament, 87);
  }
  
  public static void dumpOpenMatchSnapshots(BestBetsApi api, Tournament tournament) throws IOException {
    final List<MatchSnapshot> openSnapshots = api.loadTournamentSnapshot(tournament);
    for (MatchSnapshot snapshot: openSnapshots) {
      System.out.println(snapshot.dump());
      System.out.println();
    }
  }
  
  public static void dumpOpenMatchHistory(BestBetsApi api, Tournament tournament, int matchId) throws IOException {
    MatchHistory history = api.loadOpenMatchHistory(tournament, matchId);
    System.out.println(history.dump());
  }
  
  public static void iterateOverHistoricy(BestBetsApi api, Tournament tournament, int count) throws IOException {
    final HistoryIterator iterator = api.makeHistoryIterator(tournament, count);
    while (iterator.hasNext()) {
      final List<Match> matches = iterator.next();
      System.out.println(matches);
    }
  }

  public static void dumpHistoryMatchHistory(BestBetsApi api, Tournament tournament, int matchId) throws IOException {
    MatchHistory history = api.loadMatchHistory(tournament, matchId);
    System.out.println(history.dump());
  }
}